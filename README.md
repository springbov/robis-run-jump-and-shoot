# 2D Platformer

This is my attempt at turning the wonderful [KinematicBody2d Platformer Demo](https://github.com/godotengine/godot-demo-projects/tree/master/2d/platformer),
by the godot project, into an actual game and not just a demo.
There is also the [RigidBody2d Platformer Demo](https://github.com/godotengine/godot-demo-projects/tree/master/2d/physics_platformer) as well.
I chose the kinematicbody2d project because I thought the code
organization was a lot nicer and it had plenty of comments to
explain things inside it.

Language: GDScript

Check out the original demo on the godot asset library: https://godotengine.org/asset-library/asset/120

## Features

Features I've added on top of the original demo
- Title Screen
- Enemies now hurt you
- Screen overlay for winning a level
- Enemies can now Jump
- Death by Bottomless Pit

## Music

"Pompy" by Hubert Lamontagne (madbr) https://soundcloud.com/madbr/pompy

Various Tracks by Benjamin Burnes (tallbeard) https://tallbeard.itch.io/music-loop-bundle

## Misc Credits

- [Kenney Studio](https://kenney.nl/) for free public domain licensed assets.