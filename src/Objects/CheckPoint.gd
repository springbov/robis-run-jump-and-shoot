class_name CheckPoint
extends Area2D

signal checkpoint_reached

onready var activeSprite: Sprite = $ActiveSprite
onready var inactiveSprite: Sprite = $InactiveSprite

func _ready():
    add_to_group("checkpoints")
    self.connect("body_entered", self, "_on_CheckPoint_body_entered")

func activateCheckpoint():
    activeSprite.set_visible(true)
    inactiveSprite.set_visible(false)

func deactivateCheckpoint():
    activeSprite.set_visible(false)
    inactiveSprite.set_visible(true)

func _on_CheckPoint_body_entered(body):
    if body is Player:
        emit_signal("checkpoint_reached", self)
