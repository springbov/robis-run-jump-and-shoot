class_name Player
extends Actor

signal dead

const FLOOR_DETECT_DISTANCE = 20.0

export(int) var max_health = 5

onready var current_health = max_health
onready var platform_detector = $PlatformDetector
onready var animation_player = $AnimationPlayer
onready var shoot_timer = $ShootAnimation
onready var sprite = $Sprite
onready var hurt_cool_down_timer = $HurtCoolDown
onready var gun = sprite.get_node(@"Gun")
onready var health_label = $HUD/VBoxContainer/HealthHBox/HealthAmount
onready var coin_label = $HUD/VBoxContainer/CoinsHBox/CoinsAmount

var hurting = false
var can_move = true
var coin_count = 0

func _ready():
  health_label.text = str(current_health) + "/" + str(max_health)
  # Static types are necessary here to avoid warnings.
  var camera: Camera2D = $Camera
  camera.custom_viewport = $"../.."

# Physics process is a built-in loop in Godot.
# If you define _physics_process on a node, Godot will call it every frame.

# We use separate functions to calculate the direction and velocity to make this one easier to read.
# At a glance, you can see that the physics process loop:
# 1. Calculates the move direction.
# 2. Calculates the move velocity.
# 3. Moves the character.
# 4. Updates the sprite direction.
# 5. Shoots bullets.
# 6. Updates the animation.

# Splitting the physics process logic into functions not only makes it
# easier to read, it help to change or improve the code later on:
# - If you need to change a calculation, you can use Go To -> Function
#   (Ctrl Alt F) to quickly jump to the corresponding function.
# - If you split the character into a state machine or more advanced pattern,
#   you can easily move individual functions.
func _physics_process(_delta):
  var direction = get_direction()

  var is_jump_interrupted = Input.is_action_just_released("jump") and _velocity.y < 0.0
  _velocity = calculate_move_velocity(_velocity, direction, speed, is_jump_interrupted)

  var snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE if direction.y == 0.0 else Vector2.ZERO
  var is_on_platform = platform_detector.is_colliding()
  _velocity = move_and_slide_with_snap(
    _velocity, snap_vector, FLOOR_NORMAL, not is_on_platform, 4, 0.9, false
  )

  # When the character’s direction changes, we want to to scale the Sprite accordingly to flip it.
  # This will make Robi face left or right depending on the direction you move.
  if direction.x != 0:
    sprite.scale.x = 1 if direction.x > 0 else -1

  # We use the sprite's scale to store Robi’s look direction which allows us to shoot
  # bullets forward.
  # There are many situations like these where you can reuse existing properties instead of
  # creating new variables.
  var is_shooting = false
  if shoot_timer.is_stopped() and Input.is_action_just_pressed("shoot"):
    is_shooting = gun.shoot(sprite.scale.x)

  var animation = get_new_animation(is_shooting)
  if animation != animation_player.current_animation and shoot_timer.is_stopped():
    if is_shooting:
      shoot_timer.start()
    animation_player.play(animation)

  for i in get_slide_count():
    var collision = get_slide_collision(i)
    if collision.collider is Enemy:
      onPlayerHit()

func onPlayerHit():
  if !hurting:
    current_health -= 1
    health_label.text = str(current_health) + "/" + str(max_health)
    hurting = true
    sprite.modulate.a = 0.5
    if current_health <= 0:
      emit_signal("dead")
    else:
      hurt_cool_down_timer.start()

func restorePlayer():
  current_health = max_health
  hurting = false
  sprite.modulate.a = 1.0
  health_label.text = str(current_health) + "/" + str(max_health)


func get_direction():
  if can_move:
    return Vector2(
      Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
      -1 if is_on_floor() and Input.is_action_just_pressed("jump") else 0
    )
  else:
    return Vector2.ZERO


# This function calculates a new velocity whenever you need it.
# It allows you to interrupt jumps.
func calculate_move_velocity(
    linear_velocity,
    direction,
    speed,
    is_jump_interrupted
  ):
  var velocity = linear_velocity
  velocity.x = speed.x * direction.x
  if direction.y != 0.0:
    velocity.y = speed.y * direction.y
  if is_jump_interrupted:
    # Decrease the Y velocity by multiplying it, but don't set it to 0
    # as to not be too abrupt.
    velocity.y *= 0.6
  return velocity

func _on_HurtCoolDown_timeout():
  hurting = false
  print_debug("hurt cooldown timeout")
  hurt_cool_down_timer.stop()
  sprite.modulate.a = 1

func get_new_animation(is_shooting = false):
  var animation_new = ""
  if is_on_floor():
    animation_new = "run" if abs(_velocity.x) > 0.1 else "idle"
  else:
    animation_new = "falling" if _velocity.y > 0 else "jumping"
  if is_shooting:
    animation_new += "_weapon"
  return animation_new

func onCoinCollect():
  coin_count += 1
  coin_label.text = str(coin_count)
