extends Control

signal back_button_pressed

onready var masterSlider = $VBoxContainer/MasterVolume/MasterSlider
onready var bgmSlider    = $VBoxContainer/BGMVolume/BGMSlider
onready var sfxSlider    = $VBoxContainer/SFXVolume/SFXSlider

onready var masterIndex  = AudioServer.get_bus_index("Master")
onready var bgmIndex     = AudioServer.get_bus_index("BGM")
onready var sfxIndex     = AudioServer.get_bus_index("SFX")

# Called when the node enters the scene tree for the first time.
func _ready():
  masterSlider.value = db2linear(AudioServer.get_bus_volume_db(masterIndex))
  bgmSlider.value = db2linear(AudioServer.get_bus_volume_db(bgmIndex))
  sfxSlider.value = db2linear(AudioServer.get_bus_volume_db(sfxIndex))
  masterSlider.grab_focus()

func _on_MasterSlider_value_changed(value: float):
  AudioServer.set_bus_volume_db(masterIndex, linear2db(value))

func _on_BGMSlider_value_changed(value: float):
  AudioServer.set_bus_volume_db(bgmIndex, linear2db(value))

func _on_SFXSlider_value_changed(value: float):
  AudioServer.set_bus_volume_db(sfxIndex, linear2db(value))

func _on_BackButton_pressed():
  emit_signal("back_button_pressed")