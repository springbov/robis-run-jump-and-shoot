extends Control

signal move_on

onready var timer = $TimeToMoveOn

# Called when the node enters the scene tree for the first time.
func _ready():
  visible = false
  var stream = $VictoryFX.stream as AudioStreamOGGVorbis
  stream.set_loop(false)

func open():
  $VictoryFX.play()
  visible = true
  timer.start()

func _on_TimeToMoveOn_timeout():
  timer.stop()
  visible = false
  emit_signal("move_on")

func _on_VictoryFX_finished():
  $VictoryFX.stop()