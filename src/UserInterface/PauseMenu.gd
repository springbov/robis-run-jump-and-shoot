extends Control


onready var resume_button = $VBoxContainer/ResumeButton
onready var vbox = $VBoxContainer
onready var settings_menu = $SettingsMenu

func _ready():
	visible = false

func close():
	visible = false
	settings_menu.visible = false

func open():
	visible = true
	resume_button.grab_focus()


func _on_ResumeButton_pressed():
	get_tree().paused = false
	visible = false


func _on_QuitButton_pressed():
	get_tree().quit()

func _on_SettingsButton_pressed():
	vbox.visible = false
	settings_menu.visible = true
	pass

func _on_SettingsMenu_back_button_pressed():
	vbox.visible = true
	settings_menu.visible = false
	pass