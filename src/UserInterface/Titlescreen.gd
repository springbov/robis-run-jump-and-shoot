extends Control

onready var start_button  = $TitleMenu/StartButton
onready var settings_menu = $SettingsMenu
onready var title_menu    = $TitleMenu


# Called when the node enters the scene tree for the first time.
func _ready():
  start_button.grab_focus()
  settings_menu.visible = false

func _on_StartButton_pressed():
  get_parent().startGame()

func _on_SettingsButton_pressed():
  title_menu.visible = false
  settings_menu.visible = true
  pass

func _on_SettingsMenu_back_button_pressed():
  title_menu.visible = true
  settings_menu.visible = false
  pass
