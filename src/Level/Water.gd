tool
extends Area2D

onready var _splash = $Splash as AudioStreamPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
  var color = $ColorRect as ColorRect
  var collision = $CollisionShape2D as CollisionShape2D
  color.rect_size = collision.shape.extents * 2
  color.rect_position = collision.transform.origin - collision.shape.extents
  _splash.stream
  connect("body_entered", self, "onBodyEnter")

func onBodyEnter(body: KinematicBody2D):
  if body != null:
    _splash.play()
