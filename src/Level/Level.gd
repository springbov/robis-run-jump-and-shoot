tool
class_name Level
extends Node2D

signal cleared

export(int) var limit_left= 0
export(int) var limit_top = 0
export(int) var limit_right = 955
export(int) var limit_bottom = 690

export(String) var playerNodeName = "./Player"
export(String) var respawnPointName = "./RespawnPoint"

onready var player = get_node(playerNodeName)
onready var initial_spawn_point = get_node(respawnPointName)
onready var current_spawn_point = initial_spawn_point.position

onready var masterIndex  = AudioServer.get_bus_index("Master")

func _ready():
  var camera = player.get_node("Camera")
  camera.limit_left = limit_left
  camera.limit_top = limit_top
  camera.limit_right = limit_right
  camera.limit_bottom = limit_bottom
  player.connect("dead", self, "_on_Player_dead")
  var checkpoints = get_tree().get_nodes_in_group("checkpoints")
  for point in checkpoints:
    point.connect("checkpoint_reached", self, "onCheckPointEntered")

func _on_Clear_body_entered(body):
  if (body.get_instance_id() == player.get_instance_id()):
    player.can_move = false
    emit_signal("cleared")

func _on_DeathPlane_body_entered(body: Node):
  if body is Player:
    _on_Player_dead()

func _on_Player_dead():
  if current_spawn_point != null:
    player.position = current_spawn_point
  else:
    player.position = Vector2.ZERO
  player.restorePlayer()

func onCheckPointEntered(checkPoint: CheckPoint):
  get_tree().call_group_flags(2, "checkpoints", "deactivateCheckpoint")
  checkPoint.activateCheckpoint()
  current_spawn_point = checkPoint.position

func onWaterEnter(body):
  if body is Actor:
    body.enteredWater()
  if body is Player:
    AudioServer.set_bus_effect_enabled(masterIndex, 0, true)

func onWaterExit(body):
  if body is Actor:
    body.exitedWater()
  if body is Player:
    AudioServer.set_bus_effect_enabled(masterIndex, 0, false)
