extends Node
# This class contains controls that should always be accessible, like pausing
# the game or toggling the window full-screen.

var packedLevels = [
  preload("res://src/Level/Level1.tscn"),
  preload("res://src/Level/WinterLevel.tscn"),
  preload("res://src/Level/WaterLevel.tscn"),
  preload("res://src/Level/FirstDesignedLevel.tscn"),
  preload("res://src/Level/Level5.tscn"),
  preload("res://src/Level/Level6-Seth.tscn"),
  preload("res://src/Level/Level7-HotGarbage.tscn"),
  preload("res://src/Level/Level8-Fall.tscn"),
  preload("res://src/Level/Level9-Fleas.tscn"),
  # preload("res://src/Level/Level.tscn")
]

var currentLevelIndex = 0

var currentLevel

# The "_" prefix is a convention to indicate that variables are private,
# that is to say, another node or script should not access them.
onready var _pause_menu = $InterfaceLayer/PauseMenu
onready var _credits = $InterfaceLayer/Credits
onready var _title_music = $"Title Screen/BGM" as AudioStreamPlayer

func _init():
  OS.min_window_size = OS.window_size
  OS.max_window_size = OS.get_screen_size()

func _ready():
  randomize()

func _unhandled_input(event):
  if event.is_action_pressed("toggle_fullscreen"):
    OS.window_fullscreen = not OS.window_fullscreen
    get_tree().set_input_as_handled()
  # The GlobalControls node, in the Stage scene, is set to process even
  # when the game is paused, so this code keeps running.
  # To see that, select GlobalControls, and scroll down to the Pause category
  # in the inspector.
  elif event.is_action_pressed("toggle_pause"):
    var tree = get_tree()
    tree.paused = not tree.paused
    if tree.paused:
      _pause_menu.open()
    else:
      _pause_menu.close()
    get_tree().set_input_as_handled()

func _on_Player_dead():
  get_tree().reload_current_scene()

func _on_LevelClearedOverlay_move_on():
  currentLevelIndex += 1
  self.remove_child(currentLevel)
  currentLevel.queue_free()
  if currentLevelIndex >= packedLevels.size():
    _credits.visible = true
    _title_music.play()
    pass
  else:
    currentLevel = packedLevels[currentLevelIndex].instance()
    self.add_child(currentLevel)
    currentLevel.connect("cleared", self, "onWinConditionMet")

func startGame():
  $"Title Screen".hide()
  _title_music.stop()
  currentLevelIndex = 0
  currentLevel = packedLevels[currentLevelIndex].instance()
  currentLevel.connect("cleared", self, "onWinConditionMet")
  self.add_child(currentLevel)

func onWinConditionMet():
  $InterfaceLayer/LevelClearedOverlay.open()
