extends Control

onready var timer = $tim

# Called when the node enters the scene tree for the first time.
func _ready():
  pauseTimer()
  pass # Replace with function body.

func _on_Timer_timeout():
  get_tree().quit()

func _on_Credits_hide():
  pauseTimer()

func _on_Credits_visibility_changed():
  if visible:
    timer.start();

func pauseTimer():
  if !visible:
    timer.stop()
